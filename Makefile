DOCROOT = /srv/http/vhosts/git.archlinux.org/public
CGI_BIN = /srv/http/vhosts/git.archlinux.org/cgi-bin
CGIT_CONFIG = /etc/cgitrc
CGIT_UWSGI = /etc/uwsgi/cgit.ini

YEAR = $(shell date +%Y)

build: cgit footer.html

cgit:
	make -C cgit/

%.html: %.html.in
	sed "s/%YEAR%/$(YEAR)/" $< >$@

clean:
	rm -f footer.html

install: build
	install -Dm0644 archlogo.gif $(DOCROOT)/archlogo.gif
	install -Dm0644 archlogo.png $(DOCROOT)/archlogo.png
	install -Dm0644 archnavbar.css $(DOCROOT)/archnavbar.css
	install -Dm0644 cgit.css $(DOCROOT)/cgit.css
	install -Dm0644 favicon.ico $(DOCROOT)/favicon.ico
	install -Dm0644 robots.txt $(DOCROOT)/robots.txt
	install -Dm0755 cgit/cgit $(CGI_BIN)/cgit.cgi
	install -Dm0755 arch-bugs-links.sh $(CGI_BIN)/arch-bugs-links.sh
	install -Dm0644 footer.html $(CGI_BIN)/footer.html
	install -Dm0644 header.html $(CGI_BIN)/header.html
	install -Dm0755 syntax-highlighting.sh $(CGI_BIN)/syntax-highlighting.sh
	install -Dm0644 cgitrc $(CGIT_CONFIG)
	install -Dm0755 about-formatting.sh $(CGI_BIN)/about-formatting.sh
	install -Dm0755 html-converters/txt2html $(CGI_BIN)/html-converters/txt2html
	install -Dm0755 html-converters/md2html $(CGI_BIN)/html-converters/md2html
	install -Dm0755 html-converters/resources/markdown.pl $(CGI_BIN)/html-converters/resources/markdown.pl
	install -Dm0644 cgit.uwsgi $(CGIT_UWSGI)

.PHONY: build cgit clean install
